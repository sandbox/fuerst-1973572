This module provides a node template mechanism. "Template" here does mean a
sample node that has already some form elements filled and can be used to create
new nodes based on it. It especially does not mean a template in the context of
theming the output of a node.

This is the workflow: When adding a node you may click a link "Create from template" which let you choose from a list of nodes marked as template. After choosing a template node you get a node edit form prefilled with content from the template. Marking a node as to be used as a template
is done by checking a box at the "Node Template" setting in the node.

The module uses the Node Clone module (http://drupal.org/project/node_clone) to create nodes from templates. The difference in the workflows is: Node Clone module allows you to clone existing nodes whereas this module provides a pre-populated list of templates at node creation time. So this is just an alternate workflow which may ease the work of content editors.

Template nodes can be choosen from the "Create [type]" form using the
"Create from template" link.

The module respects permissions provided by the Node Clone module.