<?php

/**
 * @file
 * Use the Node Clone module to make an node template mechanism available.
 */

/**
 * Implements hook_menu().
 */
function node_clone_templates_menu() {
  // Stolen from node_menu() to get one menu item per existing content type.
  node_type_cache_reset();
  foreach (node_type_get_types() as $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $items['node/add/' . $type_url_str . '/choose_template'] = array(
      'title' => 'Create from template',
      'title callback' => 'check_plain',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('node_clone_templates_choose_template', $type->type),
      'access callback' => 'node_clone_templates_access_template',
      'access arguments' => array($type->type),
      'type' => MENU_LOCAL_ACTION,
    );
  }

  return $items;
}

/**
 * Check basic permissions clone/node creation permissions.
 *
 * @param string $type
 *   Machine readable name of content type.
 *
 * @return bool
 *   TRUE for access allowed, FALSE for access not allowed.
 */
function node_clone_templates_access_template($type) {
  // Check basic clone/node creation permissions only because there is no
  // node object available at this point.
  return clone_is_permitted($type) && (user_access('clone node') || user_access('clone own nodes')) && node_access('create', $type);
}

/**
 * Implements hook_form_alter().
 *
 * Add the template settings to every Node form.
 */
function node_clone_templates_form_node_form_alter(&$form, &$form_state) {
  // Check if content type is allowed to be cloned.
  if (!node_clone_templates_access_template($form['type']['#value'])) {
    return;
  }

  $form['node_clone_templates'] = array(
    '#type' => 'fieldset',
    '#title' => 'Node Clone Template',
    '#group' => 'additional_settings',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['node_clone_templates']['use_as_template'] = array(
    '#type' => 'checkbox',
    '#title' => 'Use as template',
    '#description' => t('When checked the Node is available as template when creating other nodes of this type.'),
    '#default_value' => node_clone_templates_is_template($form['nid']['#value']),
  );

  // Add submit handler.
  $form['actions']['submit']['#submit'][] = 'node_clone_templates_form_node_form_submit';
}

/**
 * Custom submit handler for Node forms.
 */
function node_clone_templates_form_node_form_submit($form, &$form_state) {
  // Should not happen but check for the existence of the form element first.
  if (!isset($form_state['values']['use_as_template'])) {
    return;
  }

  // Save template record to database or remove.
  $is_template = node_clone_templates_is_template($form_state['node']->nid);
  if ($form_state['values']['use_as_template'] && !$is_template) {
    db_insert('node_clone_templates')
      ->fields(array(
        'nid' => $form_state['node']->nid,
        'type' => $form_state['node']->type,
        ))
      ->execute();
  }
  elseif (!$form_state['values']['use_as_template'] && $is_template) {
    db_delete('node_clone_templates')
      ->condition('nid', $form_state['node']->nid)
      ->execute();
  }
}

/**
 * Implements hook_node_delete().
 */
function node_clone_templates_node_delete($node) {
  // Remove template record for the deleted node.
  db_delete('node_clone_templates')
    ->condition('nid', $node->nid)
    ->execute();
}

/**
 * Implements hook_node_type_delete().
 */
function node_clone_templates_node_type_delete($info) {
  // Remove all template records for the deleted content type.
  db_delete('node_clone_templates')
    ->condition('type', $info->type)
    ->execute();
}

/**
 * Form for choosing a template.
 *
 * @param string $type
 *   The machine name of a content type. Used to allow only templates of a
 *   certain content type.
 */
function node_clone_templates_choose_template($form, $form_state, $type) {
  $templates = node_clone_templates_get_templates($type);
  $type_info = node_type_get_type($type);

  if (!count($templates)) {
    drupal_set_message(t('No templates available for the content type %type. To create nodes of other types go back to !link and choose the appropriate type first.',
                        array('%type' => $type_info->name, '!link' => l(t('Add content'), 'node/add'))));
    return array();
  }

  $form['template_nid'] = array(
    '#type' => 'select',
    '#title' => t('Choose template'),
    '#description' => t('Listed are templates of content type %type. To create nodes of other types go back to !link and choose the appropriate type first.',
                        array('%type' => $type_info->name, '!link' => l(t('Add content'), 'node/add'))),
    '#options' => $templates,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create from template'),
  );

  return $form;
}

/**
 * Submit handler for node_clone_templates_choose_template().
 */
function node_clone_templates_choose_template_submit($form, &$form_state) {
  if (empty($form_state['values']['template_nid'])) {
    continue;
  }

  // Redirect to clone the choosen template node.
  drupal_goto(sprintf('node/%d/clone', $form_state['values']['template_nid']));
}

/**
 * Check if a node is uses as template.
 *
 * @param int $nid
 *   Node ID of the node.
 *
 * @return bool
 *   TRUE if used as template, FALSE otherwise.
 */
function node_clone_templates_is_template($nid) {
  if ($nid < 1) {
    return;
  }

  $result = db_query('SELECT nid FROM {node_clone_templates} WHERE nid = :nid',
              array(':nid' => $nid));

  if ($result->rowCount()) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Get all Nodes usable as template for a given content type.
 *
 * @param string $type
 *   Machine name of the content type as used in node.type.
 *
 * @return array
 *   Array with Node IDs.
 */
function node_clone_templates_get_templates($type) {
  $result = db_query('SELECT nct.nid, n.title, n.status, n.uid, n.type FROM {node_clone_templates} nct LEFT JOIN {node} n USING(nid) WHERE nct.type LIKE :type ORDER BY n.title',
              array(':type' => $type));

  $templates = array();
  while ($node = $result->fetchObject()) {
    // Check if current user is allowed to clone a node. For this to check
    // clone_access_cloning() and node_access() expects a node object.
    // To avoid a node_load on every node the node object used here is
    // assembled by the database call above. This though does not allow any
    // other module to modify its values using hook_noad_load() or the like.
    // Feel free to file an issue if this is not what you expect.
    if (clone_access_cloning($node)) {
      $templates[$node->nid] = $node->title;
    }
  }
  return $templates;
}
